$( document ).ready(function() {

  var switchDetails,
  SwitchBoxControls = {
    defaults: {
      switchstats : "OFF",
      switchstatus : "swit-status",
      switchButton : "switchbutton",
      switchBox : "switchbox",
      switchstatmsg : "welcome",
      chCapture : "capture"
    },

    init: function() {
      switchDetails =this.defaults;
      this.bindUIActions();
    },

    bindUIActions: function(){

      SwitchBoxControls.getbyID(switchDetails.switchButton).on("click",function(){
        SwitchBoxControls.statchange();
      });

      SwitchBoxControls.getbyID(switchDetails.chCapture).on("click",function(){
        SwitchBoxControls.toggleSwitch();
      });
    },

    statchange: function(){
      if(switchDetails.switchstats == "OFF"){
        switchDetails.switchstats = "ON";
        SwitchBoxControls.getbyID(switchDetails.switchBox).css("background-color","green");
      }
      else {
        switchDetails.switchstats = "OFF";
        SwitchBoxControls.getbyID(switchDetails.switchBox).css("background-color","");
      }
      SwitchBoxControls.updatestat(switchDetails.switchstats);
    },

    updatestat: function(stat){
      text = "Switch "+ stat;
      SwitchBoxControls.getbyID(switchDetails.switchstatus).html(text);
      if (stat == "ON"){
        SwitchBoxControls.addmsg();
      }
      else {
        SwitchBoxControls.getbyID(switchDetails.switchstatmsg).html("");
        SwitchBoxControls.toggleRowColors();
      }
    },

    addmsg: function(){
      SwitchBoxControls.getbyID(switchDetails.switchstatmsg).html("Hello! The switch is on");
      setTimeout(function(){ SwitchBoxControls.getbyID(switchDetails.switchstatmsg).html(""); }, 5000);
      SwitchBoxControls.toggleRowColors();
    },

    toggleRowColors: function(){
      if($(".new-switch").length>0)
       {
        SwitchBoxControls.getbyID("rowcont").removeClass("new-switch");
       }
       else
       {
        SwitchBoxControls.getbyID("rowcont").addClass("new-switch");
       }
    },

    toggleSwitch: function(){

      if(SwitchBoxControls.getbyID(switchDetails.chCapture).prop("checked") == true){
        SwitchBoxControls.getbyID(switchDetails.switchButton).css("background-color", "#d35400");
      }
      else {
        SwitchBoxControls.getbyID(switchDetails.switchButton).css("background-color", "");
      }
    },

    getbyID: function(element){
      return $("#"+element);
    }


  };

  SwitchBoxControls.init();
});