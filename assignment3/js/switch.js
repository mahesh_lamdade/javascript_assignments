window.onload = function ()
{
  blackColor = "#000";
  grayColor = "#808080";
  switchstats = "OFF";
  switchstatus = document.getElementById("swit-status");
  switchButton = document.getElementById("switchbutton");
  switchBox = document.getElementById("switchbox");
  switchstatmsg = document.getElementById("welcome");
  chCapture = document.getElementById("capture");
  switchButton.onclick = function(){
    statchange();
  }
  chCapture.onclick = function () {
    toggleSwitch();
  }
}

function statchange()
{
  if(switchstats == "OFF")
  {
    switchstats = "ON";
    switchBox.style.backgroundColor = "green";
  }
  else
  {
    switchstats = "OFF";
    switchBox.style.backgroundColor = "";
  }
  updatestat(switchstats);
}

function updatestat(stat)
{
  text = "Switch "+ stat;
  switchstatus.innerHTML = text;
  if (stat == "ON")
  {
    addmsg();
  }
  else
  {
    switchstatmsg.innerHTML = "";
    toggleRowColors(grayColor,blackColor);
  }
}

function addmsg()
{
  switchstatmsg.innerHTML = "Hello! The switch is on";
  setTimeout(function(){ switchstatmsg.innerHTML = ""; }, 5000);
  toggleRowColors(blackColor,grayColor);
}

function toggleRowColors(firstcolor,secondcolor)
{
  var uls = document.getElementById("rows");
  lis = uls.getElementsByTagName("li");
  for(i=0;i<lis.length;i++)
  {
    if(i%2==0)
    {
      lis[i].style.backgroundColor = firstcolor;
    }
    else
    {
      lis[i].style.backgroundColor = secondcolor;
    }
  }
}

function toggleSwitch()
{
  if(chCapture.checked)
  {
    switchButton.style.backgroundColor = "#d35400";
  }
  else
  {
    switchButton.style.backgroundColor = "";
  }
}