window.onload = function ()
{
  var switchDetails,
  SwitchBoxControls = {
    defaults: {
      blackColor : "#000",
      grayColor :"#808080",
      switchstats : "OFF",
      switchstatus : "swit-status",
      switchButton : "switchbutton",
      switchBox : "switchbox",
      switchstatmsg : "welcome",
      chCapture : "capture"
    },

    init: function(){
      switchDetails =this.defaults;
      this.bindUIActions();
    },

    bindUIActions: function(){

      SwitchBoxControls.getbyID(switchDetails.switchButton).onclick = function(){
        SwitchBoxControls.statchange();
      };
      SwitchBoxControls.getbyID(switchDetails.chCapture).onclick = function () {
        SwitchBoxControls.toggleSwitch();
      }
    },

    statchange: function(){
      if(switchDetails.switchstats == "OFF"){
        switchDetails.switchstats = "ON";
        SwitchBoxControls.getbyID(switchDetails.switchBox).style.backgroundColor = "green";
      }
      else {
        switchDetails.switchstats = "OFF";
        SwitchBoxControls.getbyID(switchDetails.switchBox).style.backgroundColor = "";
      }
      SwitchBoxControls.updatestat(switchDetails.switchstats);
    },

    updatestat: function(stat){
      text = "Switch "+ stat;
      SwitchBoxControls.getbyID(switchDetails.switchstatus).innerHTML = text;
      if (stat == "ON"){
        SwitchBoxControls.addmsg();
      }
      else {
        SwitchBoxControls.getbyID(switchDetails.switchstatmsg).innerHTML = "";
        SwitchBoxControls.toggleRowColors(switchDetails.grayColor,switchDetails.blackColor);
      }
    },

    addmsg: function(){
      SwitchBoxControls.getbyID(switchDetails.switchstatmsg).innerHTML = "Hello! The switch is on";
      setTimeout(function(){ SwitchBoxControls.getbyID(switchDetails.switchstatmsg).innerHTML = ""; }, 5000);
      SwitchBoxControls.toggleRowColors(switchDetails.blackColor,switchDetails.grayColor);
    },

    toggleRowColors: function(firstcolor,secondcolor){
      var uls = SwitchBoxControls.getbyID("rows");
      lis = uls.getElementsByTagName("li");
      for(i=0;i<lis.length;i++)
      {
        if(i%2==0){
          lis[i].style.backgroundColor = firstcolor;
        }
        else {
          lis[i].style.backgroundColor = secondcolor;
        }
      }
    },

    toggleSwitch: function(){
      if(SwitchBoxControls.getbyID(switchDetails.chCapture).checked){
        SwitchBoxControls.getbyID(switchDetails.switchButton).style.backgroundColor = "#d35400";
      }
      else {
        SwitchBoxControls.getbyID(switchDetails.switchButton).style.backgroundColor = "";
      }
    },

    getbyID: function(element){
      return document.getElementById(element);
    }

  };

  SwitchBoxControls.init();
}